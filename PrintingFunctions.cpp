#include <iostream>
#include "PrintingFunctions.h"
#include <iomanip>

using namespace std;

void PrintMatrix(int Matrix[14][14]) // ����� �������
{
	int i, j;
	for (i = 0; i < 14; ++i)
	{
		for (j = 0; j < 14; ++j)
		{
			cout << setw(4) <<Matrix[i][j] << " ";
		}
		cout << endl;
	}
}

void PrintSumm(int SumColumn[14]) //����� ����� � ������� ����� �����. ����.
{
	int i;
	for (i = 0; i < 14; i++)
	{
		cout << "����� � ������� " << i+1<< ": " << SumColumn[i] << endl;
	}
}

void PrintNegElement(int NegativElem) //����� ������� �������������� �������� � �������
{
	cout << "������ ������������� ������� �������: " << NegativElem << endl;
}