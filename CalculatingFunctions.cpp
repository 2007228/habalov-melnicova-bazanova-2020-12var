﻿#include <iostream>
#include <cstdlib>
#include <time.h>
#include "PrintingFunctions.h"

using namespace std;

void GenerateMatrix(int Matrix[14][14]) //Заполнение матрицы
{
	int i, j;
	srand(time(NULL));
	for (i = 0; i < 14; ++i)
	{
		for (j = 0; j < 14; ++j)
		{
			Matrix[i][j] = rand() % 21 - 10;
		}
	}
}

void SearchSumm(int Matrix[14][14]) // сумма столбца после первого отрицательного элементами   
{
	setlocale(0, "");
	int i, j;
	int SumColumn [14];
	int Sum = 0;
	int k;
	for (j = 0; j < 14; j++)
	{
		for (i = 0; i < 14; i++)
		{
			if (Matrix[i][j] < 0)
			{
				break;
			}
		}
			for (k = i + 1; k < 14; k++)
			{
				Sum = Sum + Matrix[k][j];
			}
			SumColumn[j] = Sum;
	}
	PrintSumm(SumColumn);
}

void SwitchLines(int Matrix[14][14]) //Поменять местами строки
{
	setlocale(0, "");
	int i, j, firstline, secondline, temp;

	cout<<"Первая строка для замены = "<< '\t';
	cin >> firstline;
	cout << "Втроая строка для замены = " << '\t';
	cin >> secondline;

	for (j = 0; j < 14; j++)
	{
	    temp = Matrix[firstline-1][j];
		Matrix[firstline-1][j] = Matrix[secondline-1][j];
		Matrix[secondline-1][j]= temp;
	}
	cout<<"Новая матрица "<< endl;
	PrintMatrix(Matrix);
}

